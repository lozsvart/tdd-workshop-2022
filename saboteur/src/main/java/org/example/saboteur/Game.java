package org.example.saboteur;


import org.example.saboteur.exception.RuleViolationException;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Game {

    private final PlayerNotifier playerNotifier;
    private final Set<SettingsFlag> settingsFlags;

    private final Board board = new Board();
    private final Map<String, List<Card>> hands;

    private Game(PlayerNotifier playerNotifier, GameBuilder gameBuilder) {
        this.playerNotifier = playerNotifier;
        this.settingsFlags = gameBuilder.getSettingsFlags();
        this.hands = gameBuilder.getStartingHands().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> toCards(entry.getValue())));
    }

    public static Game of(PlayerNotifier playerNotifier, GameBuilder gameBuilder) {
        return new Game(playerNotifier, gameBuilder);
    }

    public static Game of(PlayerNotifier playerNotifier) {
        return new Game(playerNotifier, GameBuilder.builder().build());
    }

    private List<Card> toCards(List<String> cards) {
        return cards.stream()
                .map(Card::of)
                .collect(Collectors.toList());
    }

    public void command(String player, String command) {

        if (command.startsWith("build:")) {
            String arguments = command.split(":")[1].trim();
            Coordinates coords = Coordinates.of(arguments.split(" ")[1]);
            Card card = Card.of(arguments.split(" ")[0]);
            if (settingsFlags.contains(SettingsFlag.HAND_CHECKING) && !hands.getOrDefault(player, List.of()).contains(card)) {
                throw new RuleViolationException();
            }
            board.putCard(coords, card);
        } else {
            playerNotifier.notify("player1", board.render());
        }
    }

}
