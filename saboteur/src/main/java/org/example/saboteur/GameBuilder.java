package org.example.saboteur;

import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Builder
@Getter
public class GameBuilder {

    @Builder.Default
    private final Set<SettingsFlag> settingsFlags = Set.of();
    @Builder.Default
    private final Map<String, List<String>> startingHands = Map.of();


}
