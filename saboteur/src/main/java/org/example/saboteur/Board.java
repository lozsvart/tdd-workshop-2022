package org.example.saboteur;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Board {

    private final Map<Coordinates, Card> boardContent = new HashMap<>();

    public String render() {
        StringBuilder result = new StringBuilder();

        // A1, B1, C1, ...
        // A2, B2, C2, ...
        String letters = "ABCDEFGHI";
        String numbers = "123456789";

        for (String number: numbers.split("")) {
            String row = "";
            for (String letter: letters.split("")) {
                Coordinates coord = Coordinates.of(letter, number);
                String fieldContent = Card.render(boardContent.get(coord));
                row = concatByLines(row, fieldContent);
            }
            result.append("\n").append(row);
        }
        int endX = getEndCoordinate(boardContent, Coordinates::getX);
        int endY = getEndCoordinate(boardContent, Coordinates::getY);

        return crop(result.substring(1), 0, 0, endX, endY);
    }


    private static Integer getEndCoordinate(Map<Coordinates, Card> boardContent, Function<Coordinates, Integer> getCoordintate) {
        return boardContent.keySet().stream()
                .map(getCoordintate)
                .map(y -> (y + 1) * 3)
                .max(Comparator.naturalOrder())
                .orElse(0);
    }

    private String crop(String base, int startX, int startY, int endX, int endY) {
        // 0, 0, 2, 2 -> 2x2

        return Arrays.stream(base.split("\n"))
                .skip(startY)
                .limit(endY - startY)
                .map(line -> line + "                   ")
                .map(line -> line.substring(startX, endX))
                .collect(Collectors.joining("\n"));

    }

    private String concatByLines(String first, String second) {
        String[] firstLines = first.split("\n");
        String[] secondLines = second.split("\n");

        List<String> lines = new LinkedList<>();
        for (int i = 0; i < Math.max(firstLines.length, secondLines.length); i++) {
            lines.add((i < firstLines.length ? firstLines[i] : "") + (i < secondLines.length ? secondLines[i] : ""));
        }
        return String.join("\n", lines);
    }

    public void putCard(Coordinates coords, Card card) {
        boardContent.put(coords, card);
    }
}
