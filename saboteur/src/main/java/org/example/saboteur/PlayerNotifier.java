package org.example.saboteur;

public interface PlayerNotifier {

    void notify(String player, String message);

}
