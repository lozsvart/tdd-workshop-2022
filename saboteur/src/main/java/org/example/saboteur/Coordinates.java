package org.example.saboteur;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Coordinates {

    private static final String LETTERS = "ABCDEFGHI";
    private static final String NUMBERS = "123456789";

    private final String value;

    private Coordinates(String value) {
        this.value = value;
    }

    public static Coordinates of(String value) {
        return new Coordinates(value);
    }

    public static Coordinates of(String letter, String number) {
        return new Coordinates(letter + number);
    }

    private String getLetter() {
        return value.substring(0, 1);
    }

    private String getNumber() {
        return value.substring(1, 2);
    }

    public int getX() {
        return LETTERS.indexOf(getLetter());
    }

    public int getY() {
        return NUMBERS.indexOf(getNumber());
    }
}
