package org.example.saboteur;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Card {

    public static final String EMPTY_CARD = "   \n   \n   ";
    private static final String CARD_BASE = """
            # #
              \s
            # #""";

    private final String content;

    private Card(String content) {
        this.content = content;
    }

    public static Card of(String content) {
        return new Card(content);
    }

    public static String render(Card card) {
        return card == null ? EMPTY_CARD : card.render();
    }

    public String render() {
        return CARD_BASE;
    }
}
