package org.example.saboteur;

import org.example.saboteur.exception.RuleViolationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.example.saboteur.SettingsFlag.HAND_CHECKING;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GameTest {

    public static final String PLAYER_1 = "player1";
    private Map<String, List<String>> notifications;

    @BeforeEach
    void setUp() {
        notifications = new HashMap<>();
    }

    @Test
    void shouldInspectEmptyBoard() {

        PlayerNotifier playerNotifier = inMemoryPlayerNotifier();
        Game game = Game.of(playerNotifier);

        game.command(PLAYER_1, "inspect: board");

        assertThat(notifications.get(PLAYER_1).get(0))
                .isEqualTo("");
    }

    @Test
    void shouldPutDownCard() {

        PlayerNotifier playerNotifier = inMemoryPlayerNotifier();
        Game game = Game.of(playerNotifier);

        game.command(PLAYER_1, "build: WSEN A1");

        game.command(PLAYER_1, "inspect: board");

        assertThat(notifications.get(PLAYER_1).get(0))
                .isEqualTo("# #\n" +
                           "   \n" +
                           "# #");
    }

    @Test
    void shouldPutDownTwoCards() {

        PlayerNotifier playerNotifier = inMemoryPlayerNotifier();
        Game game = Game.of(playerNotifier);

        game.command(PLAYER_1, "build: WSEN A1");
        game.command(PLAYER_1, "build: WSEN B1");

        game.command(PLAYER_1, "inspect: board");

        assertThat(notifications.get(PLAYER_1).get(0))
                .isEqualTo("# ## #\n" +
                           "      \n" +
                           "# ## #");
    }

    @Test
    void shouldPutDownManyCards() {

        PlayerNotifier playerNotifier = inMemoryPlayerNotifier();
        Game game = Game.of(playerNotifier);

        game.command(PLAYER_1, "build: WSEN A1");
        game.command(PLAYER_1, "build: WSEN B1");
        game.command(PLAYER_1, "build: WSEN A2");
        game.command(PLAYER_1, "build: WSEN B2");
        game.command(PLAYER_1, "build: WSEN C3");

        game.command(PLAYER_1, "inspect: board");

        assertThat(notifications.get(PLAYER_1).get(0))
                .isEqualTo(
                        "# ## #   \n" +
                        "         \n" +
                        "# ## #   \n" +
                        "# ## #   \n" +
                        "         \n" +
                        "# ## #   \n" +
                        "      # #\n" +
                        "         \n" +
                        "      # #");
    }

    @Test
    void shouldNotBuildWhenPlayerDoesNotHaveCard() {

        PlayerNotifier playerNotifier = inMemoryPlayerNotifier();

        Game game = Game.of(playerNotifier, GameBuilder.builder()
                .settingsFlags(Set.of(HAND_CHECKING))
                .build());

        assertThrows(RuleViolationException.class, () -> game.command(PLAYER_1, "build: WSEN A1"));

    }

    @Test
    void shouldBuildWhenPlayerHasCard() {

        PlayerNotifier playerNotifier = inMemoryPlayerNotifier();
        Game game = Game.of(playerNotifier, GameBuilder.builder()
                .settingsFlags(Set.of(HAND_CHECKING))
                .startingHands(Map.of(PLAYER_1, List.of("WSEN")))
                .build());

        assertDoesNotThrow(() -> game.command(PLAYER_1, "build: WSEN A1"));
    }

    private PlayerNotifier inMemoryPlayerNotifier() {
        return (player, message) -> {
            if (notifications.containsKey(player)) {
                notifications.get(player).add(message);
            } else {
                List<String> notifications = new LinkedList<>();
                notifications.add(message);
                this.notifications.put(player, notifications);
            }
        };
    }

}
