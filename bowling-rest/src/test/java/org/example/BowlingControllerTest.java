package org.example;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class BowlingControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BowlingService bowlingService;

    @Test
    void getScore_shouldReturnOK() throws Exception {
        when(bowlingService.getTotalScore()).thenReturn(101);
        when(bowlingService.getFrameScores()).thenReturn(Arrays.asList(7, 15));
        mockMvc.perform(get("/scores"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalScore", is(101)))
                .andExpect(jsonPath("$.frames[0].number", is(1)))
                .andExpect(jsonPath("$.frames[0].accumulatedScore", is(7)));
    }

}
