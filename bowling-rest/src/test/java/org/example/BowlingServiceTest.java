package org.example;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BowlingServiceTest {

    // Chicago style testing (instead of London)
    @Test
    void gutterGameShouldScoreZero() {

        BowlingService bowlingService = new BowlingService(() -> rollMany(20, 0));

        assertEquals(0, bowlingService.getTotalScore());

    }

    @Test
    void allOnesShouldScoreTwenty() {

        BowlingService bowlingService = new BowlingService(() -> rollMany(20, 1));

        assertEquals(20, bowlingService.getTotalScore());

    }

    private List<Integer> rollMany(int times, int roll) {
        return IntStream.range(0, times)
                .mapToObj(i -> roll)
                .collect(Collectors.toList());
    }

}