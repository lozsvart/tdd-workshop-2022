package org.example;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = BowlingApp.class)
@AutoConfigureMockMvc
public class BowlingAppIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BowlingRepository bowlingRepository;

    @Test
    void getScore_shouldReturnOK() throws Exception {
        when(bowlingRepository.getRolls()).thenReturn(
                Arrays.asList(3, 4, 4, 4, 10, 10, 10, 8, 0, 0, 0, 0, 2)
        );
        mockMvc.perform(get("/scores"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalScore", is(101)))
                .andExpect(jsonPath("$.frames[0].number", is(1)))
                .andExpect(jsonPath("$.frames[0].accumulatedScore", is(7)));
    }

}
