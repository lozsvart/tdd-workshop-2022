package org.example;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
@RequiredArgsConstructor
public class BowlingController {

    private final BowlingService bowlingService;

    @GetMapping("/scores")
    public Object getScore() {

        List<Integer> frameScores = bowlingService.getFrameScores();
        List<FrameResponse> frameResponses = IntStream.range(0, frameScores.size())
                .mapToObj(frameIndex -> FrameResponse.builder()
                        .number(frameIndex + 1)
                        .accumulatedScore(frameScores.get(frameIndex))
                        .build())
                .collect(Collectors.toList());
        return ScoreResponse.builder()
                .totalScore(bowlingService.getTotalScore())
                .frames(frameResponses)
                .build();
    }

    @Value
    @Builder
    private static class ScoreResponse {

        int totalScore;
        List<FrameResponse> frames;

    }

    @Value
    @Builder
    private static class FrameResponse {

        int number;
        int accumulatedScore;

    }
}
