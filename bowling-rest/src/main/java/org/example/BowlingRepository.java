package org.example;

import java.util.List;

public interface BowlingRepository {

    List<Integer> getRolls();

}
