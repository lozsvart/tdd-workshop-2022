package org.example;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BowlingService {

    private final BowlingRepository bowlingRepository;

    public Integer getTotalScore() {
        Game game = new Game();
        bowlingRepository.getRolls().forEach(game::roll);
        return game.score();
    }

    public List<Integer> getFrameScores() {
        // TODO finish
        return Collections.singletonList(7);
    }

}
