package org.example;

import java.io.PrintStream;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TableFormatter {

    private PrintStream out;

    public TableFormatter(PrintStream out) {
        this.out = out;
    }

    public void formatTable(List<List<String>> items) {
        List<Integer> widths = getWidths(items);
        String separatorLine = "+" + widths.stream()
                .map(width -> repeat("-", width + 2))
                .collect(Collectors.joining("+")) + "+" + "\n";

        List<String> rows = items.stream()
                .map(row -> "|" + IntStream.rangeClosed(0, row.size() - 1)
                        .mapToObj(columnIndex -> formatCell(leftPad(row.get(columnIndex), widths.get(columnIndex))))
                        .collect(Collectors.joining("|")) + "|" + "\n")
                .collect(Collectors.toList());

        out.println(separatorLine +
                rows.stream().collect(Collectors.joining(separatorLine)) +
                separatorLine);
    }

    private String formatCell(String string) {
        return " " + string + " ";
    }

    private String leftPad(String string, int length) {
        if (string.length() >= length) {
            return string;
        }
        return repeat(" ", length - string.length()) + string;
    }

    private static String repeat(String string, int times) {
        return IntStream.rangeClosed(1, times)
                .mapToObj(i -> string)
                .collect(Collectors.joining());
    }

    private List<Integer> getWidths(List<List<String>> items) {
        int columns = items.stream()
                .map(List::size)
                .max(Comparator.naturalOrder())
                .orElse(0);

        return IntStream.rangeClosed(0, columns - 1)
                .mapToObj(column -> IntStream.rangeClosed(0, items.size() - 1)
                        .mapToObj(row -> items.get(row).get(column).length())
                        .max(Comparator.naturalOrder())
                        .orElse(0)
                )
                .collect(Collectors.toList());
    }

}
