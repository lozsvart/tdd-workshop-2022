package org.example;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        TableFormatter tableFormatter = new TableFormatter(System.out);

        tableFormatter.formatTable(
                IntStream.rangeClosed(0, 10)
                        .mapToObj(i -> IntStream.rangeClosed(0, 10)
                                .mapToObj(j -> i == 0 && j == 0 ? "*" : i == 0 ? String.valueOf(j) : j == 0 ? String.valueOf(i) : String.valueOf(i * j))
                                .collect(Collectors.toList()))
                        .collect(Collectors.toList())
        );
    }
}