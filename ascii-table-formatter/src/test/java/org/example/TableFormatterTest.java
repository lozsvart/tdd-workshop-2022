package org.example;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.PrintStream;
import java.util.List;

class TableFormatterTest {

    @Test
    void shouldFormatTable() {
        PrintStream out = Mockito.mock(PrintStream.class);
        new TableFormatter(out).formatTable(List.of(List.of("abcde", "fghi"), List.of("123", "456")));

        Mockito.verify(out).println(
                "+-------+------+\n" +
                "| abcde | fghi |\n" +
                "+-------+------+\n" +
                "|   123 |  456 |\n" +
                "+-------+------+\n");
    }

}